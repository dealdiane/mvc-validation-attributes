﻿using MvcValidationAttributes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MvcValidationAttributes.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/

        public ActionResult Date()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Date(DateValueModel model)
        {
            return View(model);
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(HomeIndexModel model)
        {
            return View(model);
        }

        public ActionResult RequireValue()
        {
            return View();
        }

        [HttpPost]
        public ActionResult RequireValue(RequireValueModel model)
        {
            return View(model);
        }

        public ActionResult FileExt()
        {
            return View();
        }

        public ActionResult Conditional()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Conditional(ConditionalModel model)
        {
            return View();
        }
    }
}
