﻿using MvcValidationAttributes.Core.Mvc.DataAnnotations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace MvcValidationAttributes.Models
{
    public class ConditionalModel
    {
        public string AssociatedProperty { get; set; }

        [ConditionalRequire(OtherPropertyName="AssociatedProperty", OtherPropertyValue=null)]
        public string Dependent { get; set; }

        [DisplayName("DependentProperty 2")]
        [ConditionalRequire(OtherPropertyName="MustBeJohn", OtherPropertyValue="John")]
        [ConditionalRequire(OtherPropertyName="MustBeChecked", OtherPropertyValue=true)]
        public string DependentProperty2 { get; set; }
        
        public bool MustBeChecked { get; set; }

        public string MustBeJohn { get; set; }
    }
}