﻿using MvcValidationAttributes.Core.Mvc.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcValidationAttributes.Models
{
    public class RequireValueModel
    {
        [RequireValue(true)]
        public bool ShouldBeTrue { get; set; }

        [RequireValue(false)]
        public bool ShouldBeFalse { get; set; }

        [RequireValue(0)]
        public int ShouldBeZero { get; set; }

        [RequireValue(1)]
        public int ShouldBeOne { get; set; }

        [RequireValue(1.1)]
        public double ShouldBeOnePointOne { get; set; }

        [RequireValue("John", true)]
        public string ShouldBeJohn { get; set; }
    }
}