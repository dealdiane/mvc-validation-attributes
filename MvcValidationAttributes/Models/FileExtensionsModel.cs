﻿using MvcValidationAttributes.Core.Mvc.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcValidationAttributes.Models
{
    public class FileExtensionsModel
    {
        [FileExtensions]
        public HttpPostedFileBase File { get; set; }
    }
}