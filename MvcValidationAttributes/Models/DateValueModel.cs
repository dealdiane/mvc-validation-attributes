﻿using MvcValidationAttributes.Core.Mvc.DataAnnotations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace MvcValidationAttributes.Models
{
    public class DateValueModel
    {
        [LocalizedDate]
        [DisplayName("Date Format from user")]
        public DateTime MustBeDate { get; set; }

        [LocalizedDate(Format="M/d/yy", Pattern=LocalizedDateAttribute.DateTimePattern.Custom)]
        [DisplayName("M/d/yy")]
        public DateTime CustomDate { get; set; }
    }
}