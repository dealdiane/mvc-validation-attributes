﻿using MvcValidationAttributes.Core.Mvc.DataAnnotations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MvcValidationAttributes.Models
{
    public class HomeIndexModel
    {
        [Required]
        [MaxFileSize(1048576)]
        public HttpPostedFileBase File { get; set; }
    }
}