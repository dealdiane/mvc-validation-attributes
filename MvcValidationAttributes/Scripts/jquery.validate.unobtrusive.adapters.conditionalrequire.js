﻿
!function ($) {

    $.validator.unobtrusive.adapters.add(
        'conditionalrequire',
        ['otherpropertyname', 'type', 'value', 'comparer'],
        function (options) {
            options.rules['conditionalrequire'] = options.params;
            options.messages['conditionalrequire'] = options.message;
        }
    );

    $.validator.addMethod('conditionalrequire', function (value, element, params) {
        var hasRequiredValue = false,
            hasValue = false,
            that = this,
            $element = $(element),
            $form = $element.closest('form'),
            $otherProperty = $form.find('[name="' + params.otherpropertyname + '"]');

        if (!$otherProperty.length) {
            throw params.otherpropertyname + ' was not found.'
        }

        $otherProperty.each(function () {
            if ($.validator.methods['required'].call(that, $(this).val(), this)) {
                hasValue = true;
                return false;
            }
        });

        // ignore if any of the dependent property has no value
        if (!hasValue) {
            return true;
        }

        if (params.value !== void (0) && params.type !== void (0)) {
            $otherProperty.each(function () {
                if ($.validator.methods['requirevalue'].call(that, $(this).val(), this, params)) {
                    hasRequiredValue = true;
                    return false;
                }
            });

            // condition not met
            if (!hasRequiredValue) {
                return true;
            }
        }

        return $.validator.methods['required'].apply(this, arguments);
    });
}(jQuery);