﻿
!function ($) {
    $.validator.unobtrusive.adapters.add(
        'requirevalue',
        ['value', 'type', 'comparer'],
        function (options) {
            options.rules['requirevalue'] = options.params;
            options.messages['requirevalue'] = options.message;
        }
    );

    $.validator.addMethod('requirevalue', function (value, element, param) {

        var type = param.type,
            $element,
            boolValue;

        if ((value == null || value == void (0)) && param.type !== 'boolean') {
            return true;
        }

        if (type === 'boolean') {
            //debugger;
            $element = $(element);
            boolValue = !!param.value.match(/true/gi);

            if ($element.is(':checkbox,:radio')) {
                return $element.is(':checked') === boolValue;
            }

            return !!value.match(/true/gi) === boolValue;

        } else if (type === 'string') {
            if (param.comparer === 'ignorecase') {
                return value.toLowerCase() === param.value.toLowerCase();
            }

            return value === param.value;
        }
        else if (type === 'int32' || type === 'int64') {
            return parseInt(value) === parseInt(param.value);
        }
        else if (type === 'double' || type === 'float' || type === 'decimal') {
            return parseFloat(value) === parseFloat(param.value);
        }
        else {

            // extensibility
            // to register custom comparer, use $.validator.addMethod('requirevalue.<name of comparer>')
            if (param.comparer) {
                return $.validator.methods['requirevalue.' + param.comparer].call(this, val, element, rule.parameters);
            }

            return value == param.value;
        }
    });

}(jQuery);