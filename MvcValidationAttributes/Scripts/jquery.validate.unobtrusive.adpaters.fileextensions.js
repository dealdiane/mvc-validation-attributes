﻿
!function ($) {
    $.validator.unobtrusive.adapters.add(
        'fileextensions',
        ['extensions'],
        function (options) {
            options.rules['fileextensions'] = options.params;
            options.messages['fileextensions'] = options.message;
        }
    );

    $.validator.addMethod('fileextensions', function (value, element, param) {
        if (!value) {
            return true;
        }

        return !!((new RegExp('\\.(' + param.extensions.replace(/,/gi, '|') + ')$', 'gi')).test(value));
    });
}(jQuery);