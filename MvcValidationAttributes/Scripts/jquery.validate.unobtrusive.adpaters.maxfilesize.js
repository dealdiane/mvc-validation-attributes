﻿
!function ($) {
    $.validator.unobtrusive.adapters.add(
        'maxfilesize',
        ['filesize'],
        function (options) {
            options.rules['maxfilesize'] = options.params;
            options.messages['maxfilesize'] = options.message;
        }
    );

    $.validator.addMethod('maxfilesize', function (value, element, param) {

        var fso,
            filesize;

        //debugger;

        if (!value) {
            return true;
        }

        if (!$(element).is(':file')) {
            throw 'This validation only supports elements of type file.'
        }

        if (element.files) {
            filesize = element.files[0].size;
        } else {
            // browsers that doesn't support the file API. i.e. (<=IE9)
            // if correct security permissions is set in IE, the method
            // below will work.
            // For this to work: 
            //  Enable 'Initialize and script Activex controls not marked as safe for script option' under
            //  Tools -> Internet Options -> Security -> Custom Level
            try {
                fso = new ActiveXObject("Scripting.FileSystemObject");
                filesize = fso.getFile(value).size
            } catch (e) {
                filesize = -1;
            }
        }

        fileSize = parseInt(filesize || -1);

        if (isNaN(filesize) || filesize < 0) {
            // can't reliably get the file size using the browser
            return true;
        }

        return filesize <= param.filesize;
    });

}(jQuery);