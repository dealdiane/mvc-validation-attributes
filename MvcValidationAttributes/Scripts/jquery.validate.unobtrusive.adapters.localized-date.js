﻿
!function ($) {

    $.validator.unobtrusive.adapters.add(
        'localizeddate',
        ['dateformat'],
        function (options) {
            options.rules['localizeddate'] = options.params;
            options.messages['localizeddate'] = options.message;
        }
    );

    $.validator.addMethod('localizeddate', function (value, element, params) {
        var date = Date.parseExact(value, params.dateformat);
        return this.optional(element) || (date != null && !/Invalid|NaN/.test(date));
    });

    // replace the default date validator which is pretty much useless if you
    // are using non-US date formats
    $(function () {
        var dateValidate;

        if ($.validator.methods['localizeddate'].doNotReplaceDefaultDate) {
            return;
        }

        dateValidate = $.validator.methods['date'];

        $.validator.addMethod('date',
		    function (value, element) {
		        if ($(element).is('[data-val-localizeddate]')) {
		            return true;
		        }
		        return dateValidate.apply(this, arguments);
		    });
    });
}(jQuery);  