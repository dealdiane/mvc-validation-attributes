﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MvcValidationAttributes.Core.Mvc.DataAnnotations
{
    /// <summary>
    /// Specifies the required value for a property
    /// </summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = false)]
    public class RequireValueAttribute : ValidationAttribute, IClientValidatable
    {
        private const string _errorMessage = "The value of {0} field must be {1}.";
        private readonly IComparer _comparer;
        private readonly string _comparerName;

        /// <summary>
        /// Initializes a new instance of the <see cref="RequireValueAttribute"/> class.
        /// </summary>
        /// <param name="value">The target value.</param>
        public RequireValueAttribute(object value)
            : base(() => _errorMessage)
        {
            TargetValue = value;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RequireValueAttribute"/> class.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="ignoreCase">if set to <c>true</c> [ignores case].</param>
        /// <exception cref="System.ArgumentNullException">value</exception>
        public RequireValueAttribute(string value, bool ignoreCase)
            : this(value)
        {
            if (value == null)
            {
                throw new ArgumentNullException("value");
            }

            if (ignoreCase)
            {
                _comparer = StringComparer.OrdinalIgnoreCase;
                _comparerName = "ignorecase";
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RequireValueAttribute"/> class.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="comparer">The IComparer used to compare values.</param>
        /// <param name="comparerName">Name of the comparer for client-side validation.</param>
        protected RequireValueAttribute(object value, IComparer comparer, string comparerName)
            : this(value)
        {
            _comparer     = comparer;
            _comparerName = comparerName;
        }

        /// <summary>
        /// Gets or sets the target value.
        /// </summary>
        /// <value>
        /// The target value.
        /// </value>
        public virtual object TargetValue { get; set; }

        public override string FormatErrorMessage(string name)
        {
            var converter = TypeDescriptor.GetConverter(TargetValue.GetType());

            return string.Format(CultureInfo.CurrentCulture, ErrorMessageString, name, converter.ConvertToString(TargetValue));
        }

        public virtual IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            var rule = new ModelClientValidationRule
            {
                ErrorMessage = FormatErrorMessage(metadata.PropertyName),
                ValidationType = "requirevalue"
            };

            rule.ValidationParameters.Add("type", TargetValue.GetType().Name.ToLowerInvariant());
            rule.ValidationParameters.Add("value", TargetValue);
            rule.ValidationParameters.Add("comparer", _comparerName);

            yield return rule;
        }
     
        public override bool IsValid(object value)
        {
            if (TargetValue == null)
            {
                throw new ArgumentException("TargetValue property cannot be null.");
            }

            if (value == null)
            {
                return true;
            }

            var expectedType = TargetValue.GetType();
            var actualType   = value.GetType();

            if (expectedType != actualType && expectedType.IsSubclassOf(actualType))
            {
                var converter = TypeDescriptor.GetConverter(expectedType);

                if (converter.CanConvertFrom(value.GetType()))
                {
                    value = converter.ConvertFrom(value);
                }
                else
                {
                    converter = TypeDescriptor.GetConverter(value.GetType());

                    if (converter.CanConvertTo(expectedType))
                    {
                        value = converter.ConvertTo(value, expectedType);
                    }
                }
            }

            if (_comparer == null)
            {
                return TargetValue.Equals(value);
            }
            
            return _comparer.Compare(TargetValue, value) == 0;
        }
    }
}