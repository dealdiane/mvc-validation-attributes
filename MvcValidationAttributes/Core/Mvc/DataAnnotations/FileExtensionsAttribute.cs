﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MvcValidationAttributes.Core.Mvc.DataAnnotations
{
    public sealed class FileExtensionsAttribute : DataTypeAttribute, IClientValidatable
    {
        private string _extensions;

        public FileExtensionsAttribute()
            : base(DataType.Upload)
        {
            base.ErrorMessage = "The {0} field only accepts files with the following extensions: \"{1}\"";
        }

        public string Extensions
        {
            get
            {
                if (string.IsNullOrWhiteSpace(_extensions))
                {
                    return "bmp,png,jpg,jpeg,gif";
                }

                return _extensions;
            }
            set
            {
                _extensions = value;
            }
        }

        private string ExtensionsFormatted
        {
            get
            {
                return ExtensionsParsed.Aggregate((left, right) => left + ", " + right);
            }
        }

        private string ExtensionsNormalized
        {
            get
            {
                return Extensions.Replace(" ", string.Empty).Replace(".", string.Empty).ToLowerInvariant();
            }
        }

        private IEnumerable<string> ExtensionsParsed
        {
            get
            {
                return ExtensionsNormalized.Split(',').Select(e => "." + e);
            }
        }

        public override string FormatErrorMessage(string name)
        {
            return string.Format(System.Globalization.CultureInfo.CurrentCulture, base.ErrorMessageString, new object[]
			{
				name,
				ExtensionsFormatted
			});
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            var rule = new ModelClientValidationRule
            {
                ErrorMessage = FormatErrorMessage(metadata.GetDisplayName()),
                ValidationType = "fileextensions"
            };

            rule.ValidationParameters.Add("extensions", ExtensionsNormalized);
            yield return rule;
        }

        public override bool IsValid(object value)
        {
            if (value == null)
            {
                return true;
            }

            string filename;

            if (value is HttpPostedFileBase)
            {
                filename = ((HttpPostedFileBase)value).FileName;
            }
            else
            {
                filename = value as string;
            }

            return filename != null && ValidateExtension(filename);
        }

        private bool ValidateExtension(string fileName)
        {
            bool result;

            try
            {
                result = ExtensionsParsed.Contains(Path.GetExtension(fileName).ToLowerInvariant());
            }
            catch (ArgumentException)
            {
                result = false;
            }

            return result;
        }
    }
}