using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Globalization;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace MvcValidationAttributes.Core.Mvc.DataAnnotations
{
    public class LocalizedDateAttribute : ValidationAttribute, IMetadataAware, IClientValidatable
    {
        public LocalizedDateAttribute(string format)
            : base("The {0} is an invalid date.")
        {
            Format = format;

            if (!string.IsNullOrEmpty(Format))
            {
                Pattern = DateTimePattern.Custom;
            }
        }

        public LocalizedDateAttribute()
            : this(null)
        {

        }

        public enum DateTimePattern
        {
            ShortDatePattern                 = 0,
            FullDateTimePattern              = 1,
            LongDatePattern                  = 2,
            LongTimePattern                  = 3,
            MonthDayPattern                  = 4,
            RFC1123Pattern                   = 5,
            ShortTimePattern                 = 6,
            SortableDateTimePattern          = 7,
            UniversalSortableDateTimePattern = 8,
            YearMonthPattern                 = 9,
            Custom                           = 10,
            /*All                              = 11,*/
        }

        public string Format
        {
            get;
            set;
        }

        public DateTimePattern Pattern { get; set; }

        public static CultureInfo GetCulure(string[] userLanguages)
        {
            if (userLanguages != null && userLanguages.Length > 0)
            {
                var userLanguage = userLanguages[0];
                var index = userLanguage.IndexOf(';');

                if (index > 0)
                {
                    userLanguage = userLanguage.Substring(0, index);
                }

                try
                {
                    return CultureInfo.ReadOnly(new CultureInfo(userLanguage));
                }
                catch
                {
                    Debug.WriteLine("Could not create culture from language:" + userLanguage);
                }
            }

            return CultureInfo.CurrentUICulture;
        }

        public override string FormatErrorMessage(string name)
        {
            var format = HttpContext.Current == null ?  GetFormat(null) : GetFormat(new HttpRequestWrapper(HttpContext.Current.Request));

            return FormatErrorMessage(name, format);
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            string dateFormat = GetFormat(context.HttpContext.Request);

            if (string.IsNullOrEmpty(dateFormat))
            {
                throw new InvalidOperationException("Date format not set");
            }

            yield return new ModelClientValidationRule
                             {
                                 ErrorMessage         = FormatErrorMessage(metadata.PropertyName, dateFormat),
                                 ValidationType       = "localizeddate",
                                 ValidationParameters =
                                 {
                                    { "dateformat", dateFormat }
                                 }
                             };
        }

        public override bool IsValid(object value)
        {
            if (value is DateTime)
            {
                return true;
            }

            DateTime date;
            var request = new HttpRequestWrapper(HttpContext.Current.Request);

            return DateTime.TryParseExact(Convert.ToString(value), GetFormat(request), CultureInfo.InvariantCulture, DateTimeStyles.None, out date);
        }

        public void OnMetadataCreated(ModelMetadata metadata)
        {
            metadata.AdditionalValues["localized-date-format"] = GetFormat(new HttpRequestWrapper(HttpContext.Current.Request));
        }

        private string FormatErrorMessage(string name, string format)
        {
            return string.Format(CultureInfo.CurrentCulture, ErrorMessageString, name, format);
        }
        
        private string GetFormat(HttpRequestBase request)
        {
            var format = GetCulure(request.UserLanguages).DateTimeFormat;

            switch (Pattern)
            {
                case DateTimePattern.ShortDatePattern:
                    return format.ShortDatePattern;
                case DateTimePattern.FullDateTimePattern:
                    return format.FullDateTimePattern;
                case DateTimePattern.LongDatePattern:
                    return format.LongDatePattern;
                case DateTimePattern.LongTimePattern:
                    return format.LongTimePattern;
                case DateTimePattern.MonthDayPattern:
                    return format.MonthDayPattern;
                case DateTimePattern.RFC1123Pattern:
                    return format.RFC1123Pattern;
                case DateTimePattern.ShortTimePattern:
                    return format.ShortTimePattern;
                case DateTimePattern.SortableDateTimePattern:
                    return format.SortableDateTimePattern;
                case DateTimePattern.UniversalSortableDateTimePattern:
                    return format.UniversalSortableDateTimePattern;
                case DateTimePattern.YearMonthPattern:
                    return format.YearMonthPattern;
                //case DateTimePattern.All:
                //    {
                //        foreach (var formatting in format.GetAllDateTimePatterns())
                //        {
                //            return formatting;
                //        }
                //    }
                //    break;
                case DateTimePattern.Custom:
                    {
                        if (string.IsNullOrEmpty(Format))
                        {
                            throw new InvalidOperationException("Format property requires a value if pattern is set to custom.");
                        }

                        return Format;
                    }
                default:
                    throw new NotSupportedException();
            }
        }
    }
}