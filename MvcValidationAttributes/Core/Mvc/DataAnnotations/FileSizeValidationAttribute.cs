﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MvcValidationAttributes.Core.Mvc.DataAnnotations
{
    /// <summary>
    /// Specifies the maximum file size allowed for an HttpPostedFileBase
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class MaxFileSizeAttribute : ValidationAttribute, IClientValidatable
    {
        /// <summary>
        /// Gets or sets the maximum size of the file in bytes.
        /// </summary>
        /// <value>
        /// The maximum size of the file in bytes.
        /// </value>
        public int FileSize { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="MaxFileSizeAttribute"/> class.
        /// </summary>
        /// <param name="fileSize">Maximum size of the file in bytes.</param>
        public MaxFileSizeAttribute(int fileSize)
            : base(() => "The {0} field exceeds the maximum file size allowed.")
        {
            FileSize = fileSize;
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            var rule = new ModelClientValidationRule
            {
                ErrorMessage = FormatErrorMessage(metadata.PropertyName),
                ValidationType = "maxfilesize"
            };

            rule.ValidationParameters.Add("filesize", FileSize);

            yield return rule;
        }

        public override bool IsValid(object value)
        {
            if (value == null)
            {
                return true;
            }

            if (!(value is HttpPostedFileBase))
            {
                throw new InvalidOperationException("This validation attribute only supports the System.Web.HttpPostedFileBase type.");
            }

            return ((HttpPostedFileBase)value).ContentLength <= FileSize;
        }
    }
}