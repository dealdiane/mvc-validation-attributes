﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web;
using System.Web.Mvc;

namespace MvcValidationAttributes.Core.Mvc.DataAnnotations
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = true)]
    public class ConditionalRequireAttribute : ValidationAttribute
    {
        private static readonly object _empty = new object();
        private readonly IComparer _comparer;
        private readonly string _comparerName;

        public ConditionalRequireAttribute(
            [CallerMemberName]
            string propertyName = null)
        {
            OtherPropertyValue = _empty;
            PropertyName = propertyName;
        }

        public bool AllowEmptyStrings { get; set; }

        public string OtherPropertyName { get; set; }

        public object OtherPropertyValue { get; set; }

        public string PropertyName { get; set; }
        public override bool RequiresValidationContext
        {
            get
            {
                return true;
            }
        }

        public override string FormatErrorMessage(string name)
        {
            return string.Format(CultureInfo.CurrentCulture, ErrorMessageString, name);
        }

        public virtual IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            //var rule = new ModelClientValidationRule
            //{
            //    ErrorMessage = FormatErrorMessage(metadata.GetDisplayName()),
            //    ValidationType = "conditionalrequire"
            //};

            //rule.ValidationParameters.Add("otherpropertyname", OtherPropertyName);
            
            //if (TargetValue != null)
            //{
            //    foreach (var baseRule in base.GetClientValidationRules(metadata, context))
            //    {
            //        if (baseRule.ValidationType != "requirevalue")
            //        {
            //            yield return baseRule;
            //        }

            //        foreach (var parameter in baseRule.ValidationParameters)
            //        {
            //            rule.ValidationParameters.Add(parameter);
            //        }
            //    }
            //}

            //yield return rule;
            yield break;
        }

        public override bool IsValid(object value)
        {
            // if this is called, it means the validation is failed and this is called
            // from the base method so we can get the properly formatted error message.
            return false;
        }

        protected virtual IEnumerable<ConditionalRequireAttribute> GetCustomAttributes(PropertyInfo property)
        {
            return (ConditionalRequireAttribute[])property.GetCustomAttributes(typeof(ConditionalRequireAttribute), false);
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var currentProperty       = validationContext.ObjectType.GetProperty(PropertyName);
            var conditionalAttributes = GetCustomAttributes(currentProperty);
            var shouldCheckProperty   = true;

            // set flag if all conditions are met
            foreach (var conditionalAttribute in conditionalAttributes)
            {
                var otherProperty = validationContext.ObjectType.GetProperty(conditionalAttribute.OtherPropertyName);
                var actualValue   = otherProperty.GetValue(validationContext.ObjectInstance);
                var expectedValue = conditionalAttribute.OtherPropertyValue;

                // user requires that the value of associated property is null; or
                // user requires that the value can be any value as long as it's not null
                /*if ((conditionalAttribute.OtherPropertyValue != _empty && otherPropertyActualValue == null) || 
                    (conditionalAttribute.OtherPropertyValue == _empty && otherPropertyActualValue != null))*/
                if ((expectedValue == _empty && actualValue != null) ||
                    (expectedValue == null   && actualValue == null))
                {
                    continue;
                }

                // complex value checking. user requires to check against the value passed
                if (actualValue != null && expectedValue != null)
                {
                    var expectedType = expectedValue.GetType();
                    var actualType = actualValue.GetType();

                    // convert the actual value to the expected type if required
                    if (expectedType != actualType)
                    {
                        var converter = TypeDescriptor.GetConverter(expectedType);

                        if (converter.CanConvertFrom(actualValue.GetType()))
                        {
                            actualValue = converter.ConvertFrom(actualValue);
                        }
                        else
                        {
                            converter = TypeDescriptor.GetConverter(actualValue.GetType());

                            if (converter.CanConvertTo(expectedType))
                            {
                                actualValue = converter.ConvertTo(actualValue, expectedType);
                            }
                        }
                    }
                }

                if ((_comparer == null && !object.Equals(expectedValue, actualValue)) ||
                    (_comparer != null && _comparer.Compare(expectedValue, actualValue) != 0))
                {
                    shouldCheckProperty = false;
                    break;
                }
            }

            // all conditions have been met
            if (shouldCheckProperty)
            {
                if (value == null)
                {
                    // calling the base method ensure we get a properly formatted error message
                    return base.IsValid(null, validationContext);
                }

                var text = value as string;

                if (text == null || this.AllowEmptyStrings || text.Trim().Length != 0)
                {
                    // calling the base method ensure we get a properly formatted error message
                    return base.IsValid(null, validationContext);
                }
            }

            return ValidationResult.Success;
        }
    }
}