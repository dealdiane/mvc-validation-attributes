﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MvcValidationAttributes.Core.Mvc.ModelBinders
{
    public class LocalizedDateModelBinder : IModelBinder
    {
        private static DefaultModelBinder _defaultBinder;

        protected static DefaultModelBinder DefaultBinder
        {
            get
            {
                if (_defaultBinder == null)
                {
                    _defaultBinder = new DefaultModelBinder();
                }

                return _defaultBinder;
            }
        }

        public virtual object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            if (bindingContext.ModelMetadata.AdditionalValues.ContainsKey("localized-date-format"))
            {
                var valueProvider = bindingContext.ValueProvider.GetValue(bindingContext.ModelName);
                var value         = (string)valueProvider.ConvertTo(typeof(string));
                var dateFormat    = bindingContext.ModelMetadata.AdditionalValues["localized-date-format"] as string;

                if (!string.IsNullOrEmpty(dateFormat))
                {
                    bindingContext.ModelState.SetModelValue(bindingContext.ModelName, valueProvider);

                    try
                    {
                        return DateTime.ParseExact(value, dateFormat, CultureInfo.InvariantCulture);
                    }
                    catch (Exception ex)
                    {
                        bindingContext.ModelState.AddModelError(bindingContext.ModelName, ex);

                        return null;
                    }
                }
            }

            return DefaultBinder.BindModel(controllerContext, bindingContext);
        }
    }
}